import central_server
import distributed_server

print("* Informe qual servidor voce esta inicializando:\n")
print("- Servidor Central (1)")
print("- Servidor Distribuido (2)")
option = int(input("Opcao: "))
while (option < 1 or option > 2):
    option = int(input(
            "Por favor digite alguma opcao valida: "))
if (option == 1):
    parking = int(input(
            "\n[1/4] Informe o numero (1-7) do Estacionamento: "))
    while (parking < 1 or parking > 6):
        parking = int(input(
                "[1/4] Por favor digite algum Estacionamento valido: "))

    floor = int(input(
            "[2/4] Informe se e o Primeiro Andar (1) ou Andar Superior (2): "))
    while (floor < 1 or floor > 2):
        floor = int(input(
                "[2/4] Por favor digite algum Andar valido: "))
    if (floor == 1):
        floor = "first"
    elif (floor == 2):
        floor = "upper"

    first_floor_parking = int(input(
            "[3/4] Informe o numero do Estacionamento do Servidor " +
            "Distribuido do Primeiro Andar: "))
    while (first_floor_parking < 1 or first_floor_parking > 6):
        first_floor_parking = int(input(
                "[3/4] Por favor digite algum Estacionamento valido: "))

    upper_floor_parking = int(input(
            "[4/4] Informe o numero do Estacionamento do Servidor " +
            "Distribuido do Segundo Andar: "))
    while (upper_floor_parking < 1 or upper_floor_parking > 6):
        upper_floor_parking = int(input(
                "[4/4] Por favor digite algum Estacionamento valido: "))

    cs = central_server.CentralServer(parking, floor,
            first_floor_parking, upper_floor_parking)
    cs.routine()

elif (option == 2):
    parking = int(input(
            "\n[1/2] Informe o numero (1-7) do Estacionamento: "))
    while (parking < 1 or parking > 6):
        parking = int(input(
                "[1/2] Por favor digite algum Estacionamento valido: "))

    floor = int(input(
            "[2/2] Informe se e o Primeiro Andar (1) ou Andar Superior (2): "))
    while (floor < 1 or floor > 2):
        floor = int(input(
                "[2/2] Por favor digite algum Andar valido: "))
    if (floor == 1):
        floor = "first"
    elif (floor == 2):
        floor = "upper"

    ds = distributed_server.DistributedServer(parking, floor)
    ds.routine()
