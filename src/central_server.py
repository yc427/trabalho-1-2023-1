import RPi.GPIO as GPIO
import json
import datetime
import socket
import pickle
import struct
import threading

class CentralServer:

    def __init__(self, parking, floor,
            first_floor_parking, upper_floor_parking):
        self.parking = parking
        self.floor = floor
        self.first_floor_parking = first_floor_parking
        self.upper_floor_parking = upper_floor_parking

    def first_floor_lock_routine(self, none):
        self.first_floor_locked = False
        if (self.first_floor_parking == 4):
            self.first_floor_host = self.hosts[4-1][0]
            self.first_floor_lock_sck.connect((self.first_floor_host, 10561))
        elif (self.first_floor_parking != 4
                and self.first_floor_parking in range(1, 7)):
            self.first_floor_host = self.hosts[self.first_floor_parking-1]
            self.first_floor_lock_sck.connect((self.first_floor_host, 10561))
        while True:
            occupied_parking_lots = 0
            for i in range(8):
                if (self.first_floor_parking_lots[i] == 1):
                    occupied_parking_lots += 1
            if (self.first_floor_parking_lots[i] == 8):
                self.first_floor_lock_sck.send(
                        struct.pack('i', self.first_floor_locked))
                self.old_first_floor_locked = self.first_floor_locked
            if (self.first_floor_locked != self.old_first_floor_locked):
                self.first_floor_lock_sck.send(
                        struct.pack('i', self.first_floor_locked))
                self.old_first_floor_locked = self.first_floor_locked

    def entry_routine(self, none):
        self.entry_date_time = ""
        self.entry_sck.bind((self.host, 10562))
        self.entry_sck.listen(1)
        first_floor, tmp = self.entry_sck.accept()
        while True:
            self.entry_date_time = pickle.loads(first_floor.recv(1024))

    def exit_routine(self, none):
        self.exit_date_time = ""
        self.exit_sck.bind((self.host, 10563))
        self.exit_sck.listen(1)
        first_floor, tmp = self.exit_sck.accept()
        while True:
            self.exit_date_time = pickle.loads(first_floor.recv(1024))

    def first_floor_parking_lots_routine(self, none):
        self.first_floor_parking_lots = [0, 0, 0, 0, 0, 0, 0, 0]
        self.first_floor_parking_lots_sck.bind((self.host, 10564))
        self.first_floor_parking_lots_sck.listen(1)
        first_floor, tmp = self.first_floor_parking_lots_sck.accept()
        while True:
            self.first_floor_parking_lots = pickle.loads(first_floor.recv(1024))

    def upper_floor_lock_routine(self, none):
        self.upper_floor_locked = False
        if (self.upper_floor_parking == 4):
            self.upper_floor_host = self.hosts[4-1][1]
            self.upper_floor_lock_sck.connect((self.upper_floor_host, 10565))
        elif (self.upper_floor_parking != 4
                and self.upper_floor_parking in range(1, 7)):
            self.upper_floor_host = self.hosts[self.upper_floor_parking-1]
            self.upper_floor_lock_sck.connect((self.upper_floor_host, 10565))
        while True:
            occupied_parking_lots = 0
            for i in range(8):
                if (self.upper_floor_parking_lots[i] == 1):
                    occupied_parking_lots += 1
            if (self.upper_floor_parking_lots[i] == 8):
                self.upper_floor_lock_sck.send(
                        struct.pack('i', self.upper_floor_locked))
                self.old_upper_floor_locked = self.upper_floor_locked
            if (self.upper_floor_locked != self.old_upper_floor_locked):
                self.upper_floor_lock_sck.send(
                        struct.pack('i', self.upper_floor_locked))
                self.old_upper_floor_locked = self.upper_floor_locked

    def upper_floor_parking_lots_routine(self, none):
        self.upper_floor_parking_lots = [0, 0, 0, 0, 0, 0, 0, 0]
        self.upper_floor_parking_lots_sck.bind((self.host, 10566))
        self.upper_floor_parking_lots_sck.listen(1)
        upper_floor, tmp = self.upper_floor_parking_lots_sck.accept()
        while True:
            self.upper_floor_parking_lots = pickle.loads(upper_floor.recv(1024))

    def calculate_price(date_time_entry, date_time_exit):
        date_time = date_time_entry - date_time_exit
        return (0.15 * int(date_time.total_seconds() / 60))

    def menu_routine(self, none):
        option = 0
        while True:
            print("Fechar/Abrir entrada do 1 ANDAR (1)")
            print("Fechar/Abrir entrada do 2 ANDAR (2)")
            print("Visualizar horario de saida / valor pago (3)")
            option = int(input("Opcao: "))
            while (option < 1 or option > 3):
                option = input("Por favor digita alguma opcao valida: ")
            if (option == 1):
                if (self.first_floor_locked == True):
                    self.first_floor_locked == False
                elif (self.first_floor_locked == False):
                    self.first_floor_locked == True
            elif (option == 2):
                if (self.upper_floor_locked == True):
                    self.upper_floor_locked == False
                elif (self.upper_floor_locked == False):
                    self.upper_floor_locked == True
            elif (option == 3):
                print("")
                for i in self.prices:
                    print(i)
                print("")

    def routine(self):
        self.hosts = [
            "164.41.98.15", "164.41.98.28",
            "164.41.98.16", ["164.41.98.27", "164.41.98.29"],
            "164.41.98.26", "164.41.98.24"
            ]
        if (self.parking == 4):
            if (self.floor == "first"):
                self.host = self.hosts[4-1][0]
            if (self.floor == "upper"):
                self.host = self.hosts[4-1][1]
        elif (self.parking != 4
                and self.parking in range(1, 7)):
            self.host = self.hosts[self.parking-1]
        self.first_floor_lock_sck = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
        self.entry_sck = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
        self.exit_sck = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
        self.first_floor_parking_lots_sck = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
        self.upper_floor_lock_sck = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
        self.upper_floor_parking_lots_sck = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
        self.first_floor_host = None
        self.upper_floor_host = None

        self.first_floor_locked = False
        self.upper_floor_locked = False
        self.first_floor_parking_lots = [0, 0, 0, 0, 0, 0, 0, 0]
        self.upper_floor_parking_lots = [0, 0, 0, 0, 0, 0, 0, 0]
        self.entry_date_time = ""
        self.exit_date_time = ""
        self.old_first_floor_locked = False
        self.old_upper_floor_locked = False
        self.old_first_floor_parking_lots = [0, 0, 0, 0, 0, 0, 0, 0]
        self.old_upper_floor_parking_lots = [0, 0, 0, 0, 0, 0, 0, 0]
        self.old_entry_date_time = ""
        self.old_exit_date_time = ""

        self.date_times = [
            ["", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", ""]
            ]
        self.prices = []

        first_floor_lock_routine_thread = threading.Thread(
                target=self.first_floor_lock_routine,
                args=(self,))
        first_floor_lock_routine_thread.start()
        entry_routine_thread = threading.Thread(
                target=self.entry_routine,
                args=(self,))
        entry_routine_thread.start()
        exit_routine_thread = threading.Thread(
                target=self.exit_routine,
                args=(self,))
        exit_routine_thread.start()
        first_floor_parking_lots_routine_thread = threading.Thread(
                target=self.first_floor_parking_lots_routine,
                args=(self,))
        first_floor_parking_lots_routine_thread.start()
        upper_floor_lock_routine_thread = threading.Thread(
                target=self.upper_floor_lock_routine,
                args=(self,))
        upper_floor_lock_routine_thread.start()
        upper_floor_parking_lots_routine_thread = threading.Thread(
                target=self.upper_floor_parking_lots_routine,
                args=(self,))
        upper_floor_parking_lots_routine_thread.start()

        menu_routine_thread = threading.Thread(
                target=self.menu_routine,
                args=(self,))
        menu_routine_thread.start()


        while True:
            for i in range(8):
                if (self.old_first_floor_parking_lots[i] == 0 and
                        self.first_floor_parking_lots[i] == 1 and
                        self.entry_date_time != self.old_entry_date_time):
                    self.date_times[0][i] = self.entry_date_time
                    self.old_first_floor_parking_lots = (
                            self.first_floor_parking_lots)
                    self.old_entry_date_time = self.entry_date_time
                if (self.old_upper_floor_parking_lots[i] == 0 and
                        self.upper_floor_parking_lots[i] == 1 and
                        self.entry_date_time != self.old_entry_date_time):
                    self.date_times[1][i] = self.entry_date_time
                    self.old_upper_floor_parking_lots = (
                            self.upper_floor_parking_lots)
                    self.old_entry_date_time = self.entry_date_time
                if (self.old_first_floor_parking_lots[i] == 1 and
                        self.first_floor_parking_lots[i] == 0 and
                        self.exit_date_time != self.old_exit_date_time):
                    self.prices.append({
                            date_time: self.exit_date_time,
                            price: self.calculate_price(
                            self.date_times[0][i], self.exit_date_time)
                            })
                    self.old_first_floor_parking_lots = (
                            self.first_floor_parking_lots)
                    self.old_exit_date_time = self.exit_date_time
                if (self.old_upper_floor_parking_lots[i] == 1 and
                        self.upper_floor_parking_lots[i] == 0 and
                        self.exit_date_time != self.old_exit_date_time):
                    self.prices.append({
                            date_time: self.exit_date_time,
                            price: self.calculate_price(
                            self.date_times[1][i], self.exit_date_time)
                            })
                    self.old_first_floor_parking_lots = (
                            self.first_floor_parking_lots)
                    self.old_exit_date_time = self.exit_date_time
                    
