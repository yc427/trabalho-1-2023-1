import RPi.GPIO as GPIO
import os
import json
import datetime
import socket
import time
import pickle
import threading

class DistributedServer:

    def __init__(self, parking, floor):
        self.parking = parking
        self.floor = floor
        self.set_gpios()

    def set_gpios(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        if (self.parking == 4):
            if (self.floor == "first"):
                json_file_path = os.path.join(dir_path,
                        'car_parking_gpios', '4th_first_floor.json')
                with open(json_file_path, 'r') as json_file:
                    self.values = json.load(json_file)
            elif (self.floor == "upper"):
                json_file_path = os.path.join(dir_path,
                        'car_parking_gpios', '4th_upper_floor.json')
                with open(json_file_path, 'r') as json_file:
                    self.values = json.load(json_file)
        elif (self.parking != 4 and self.parking in range(1, 7)):
            if (self.floor == "first"):
                json_file_path = os.path.join(dir_path,
                        'car_parking_gpios', 'first_floor.json')
                with open(json_file_path, 'r') as json_file:
                    self.values = json.load(json_file)
            elif (self.floor == "upper"):
                json_file_path = os.path.join(dir_path,
                        'car_parking_gpios', 'upper_floor.json')
                with open(json_file_path, 'r') as json_file:
                    self.values = json.load(json_file)

        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        # Reset GPIOs
        for pin in range (0, 28):
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, GPIO.LOW)
        for pin in self.values["outputs"].values():
            GPIO.setup(pin["gpio"], GPIO.OUT)
        for pin in self.values["inputs"].values():
            GPIO.setup(pin["gpio"], GPIO.IN)

    def read_gpio(self, item):
        pin = self.values["inputs"][item]["gpio"]
        self.values["inputs"][item]["value"] = GPIO.input(pin)
        return self.values["inputs"][item]["value"]

    def write_gpio(self, item, value):
        self.values["outputs"][item]["value"] = value
        GPIO.output(self.values["outputs"][item]["gpio"], value)

    def entry_routine(self, address, none):
        central_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        central_server.connect((address[0], 10562))
        while True:
            if (self.read_gpio("SINAL_DE_LOTADO_FECHADO") == GPIO.HIGH):
                self.write_gpio("MOTOR_CANCELA_ENTRADA", GPIO.LOW)
                continue
            if (self.read_gpio("SENSOR_ABERTURA_CANCELA_ENTRADA") == GPIO.HIGH):
                central_server.send(pickle.dumps(datetime.datetime.now()))
                self.write_gpio("MOTOR_CANCELA_ENTRADA", GPIO.HIGH)
                time.sleep(4)
            else:
                self.write_gpio("MOTOR_CANCELA_ENTRADA", GPIO.LOW)

    def exit_routine(self, address, none):
        central_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        central_server.connect((address[0], 10563))
        while True:
            if (self.read_gpio("SENSOR_ABERTURA_CANCELA_SAIDA") == GPIO.HIGH):
                central_server.send(pickle.dumps(datetime.datetime.now()))
                self.write_gpio("MOTOR_CANCELA_SAIDA", GPIO.HIGH)
                time.sleep(4)
            else:
                self.write_gpio("MOTOR_CANCELA_SAIDA", GPIO.LOW)

    def parking_lots_routine(self, address, none):
        old_state = [0, 0, 0, 0, 0, 0, 0, 0],
        parking_lots = [0, 0, 0, 0, 0, 0, 0, 0]
        central_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if (self.floor == "first"):
            central_server.connect((address[0], 10564))
        elif (self.floor == "upper"):
            central_server.connect((address[0], 10566))
        while True:
            for i in range(8):
                binary = bin(i)[2:].zfill(3)
                self.write_gpio("ENDERECO_01", int(binary[2]))
                self.write_gpio("ENDERECO_02", int(binary[1]))
                self.write_gpio("ENDERECO_03", int(binary[0]))
                time.sleep(0.1)
                parking_lots[i] = self.read_gpio("SENSOR_DE_VAGA")
            if (parking_lots != old_state):
                central_server.send(pickle.dumps(parking_lots))
                old_state = parking_lots

    def routine(self):
        hosts = [
            "164.41.98.15", "164.41.98.28",
            "164.41.98.16", ["164.41.98.27", "164.41.98.29"],
            "164.41.98.26", "164.41.98.24"
            ]
        distributed_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if (self.parking == 4 and self.floor == "first"):
            distributed_server.bind((hosts[4-1][0], 10561))
        elif (self.parking == 4 and self.floor == "upper"):
            distributed_server.bind((hosts[4-1][1], 10565))
        elif (self.parking != 4 and self.parking in range(1, 7) and
                self.floor == "first"):
            distributed_server.bind((hosts[self.parking-1], 10561))
        elif (self.parking != 4 and self.parking in range(1, 7) and
                self.floor == "upper"):
            distributed_server.bind((hosts[self.parking-1], 10565))
        distributed_server.listen()
        central_server, address = distributed_server.accept()
        entry_routine_thread = threading.Thread(
                target=self.entry_routine, args=(self, address))
        exit_routine_thread = threading.Thread(
                target=self.exit_routine, args=(self, address))
        parking_lots_routine_thread = threading.Thread(
                target=self.parking_lots_routine, args=(self, address))
        if (self.floor == "first"):
            time.sleep(1)
            entry_routine_thread.start()
            exit_routine_thread.start()
            parking_lots_routine_thread.start()
        elif (self.floor == "upper"):
            time.sleep(1)
            parking_lots_routine_thread.start()
        while True:
            self.write_gpio("SINAL_DE_LOTADO_FECHADO",
                    central_server.recv(1024))
            
